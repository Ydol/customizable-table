import { Table, TableBody, TableHead, TableRow } from "@mui/material";
import { CellHeader, styleSubHeader } from "./table.css";
import { Cell, CellHead, CellSubHead } from "./TableRedefineCell";
import { Magasin } from "../data";

interface Props {
  index: number;
  magasins: Magasin[];
  context: {
    context1: { display: string } | null;
    context1Et2: { display: string } | null;
    context3: { display: string } | null;
  };
}

const MagasinCell = (props: Props) => {
  const { index, magasins, context } = props;
  return (
    <Cell sx={{ ...CellHeader }}>
      <Table>
        <TableHead>
          <TableRow>
            <CellHead sx={{ ...context.context1 }}></CellHead>
            <CellHead sx={{ ...context.context3 }}>Magasin{index + 1}</CellHead>
            <CellHead sx={{ ...context.context1 }}></CellHead>
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow sx={{ ...styleSubHeader }}>
            <CellSubHead sx={{ ...context.context1 }}>
              Inv {magasins[index]?.name} colis
            </CellSubHead>
            <CellSubHead width={"10px"} sx={{ ...context.context1 }}>
              Inv {magasins[index]?.name} piéce
            </CellSubHead>
            <CellSubHead sx={{ ...context.context3 }}>
              Inv {magasins[index]?.name} conca
            </CellSubHead>
            <CellSubHead sx={{ ...context.context3 }}>
              Client {magasins[index]?.name}
            </CellSubHead>
            <CellSubHead
              sx={{ ...context.context3, backgroundColor: "#fff1d0" }}
            >
              Projet UA {magasins[index]?.name}
            </CellSubHead>
          </TableRow>
          {magasins?.map((item) => {
            return (
              <TableRow sx={{ height: "90px" }}>
                <Cell sx={{ ...context.context1 }}>{item.invColis}</Cell>
                <Cell sx={{ ...context.context1 }}>{item.invPiece}</Cell>
                <Cell sx={{ ...context.context3 }}>{item.invConca}</Cell>
                <Cell sx={{ ...context.context1 }}></Cell>
                <Cell
                  sx={{ ...context.context3, backgroundColor: "#fff1d0" }}
                ></Cell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </Cell>
  );
};

export default MagasinCell;
