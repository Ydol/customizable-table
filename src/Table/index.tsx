import * as React from "react";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableContainer from "@mui/material/TableContainer";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import ProductCell from "./ProductCell";
import MagasinCell from "./MagasinCell";
import TransfertCellEtRetour from "./TransfertEtRetourCell";
import { Model } from "../data";

interface Props {
  data: Model[];
  context?: number[];
  afficherTransfertEtRetour: {
    retour: boolean;
    transfert: boolean;
  };
}

export default function ColumnGroupingTable(props: Props) {
  const { data, context, afficherTransfertEtRetour } = props;
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const getNberOfMagasin = (): number[] => {
    let max = 0;
    let tab = [];
    for (let i = 0; i < data.length; i++) {
      if (data[i].magasins.length > max) max = data[i].magasins.length;
    }

    for (let index = 0; index < max; index++) {
      tab[index] = index;
    }
    return tab;
  };

  const displayCell = (
    valueContext: number[],
    context?: number[]
  ): { display: string } | null => {
    const value =
      context && context.length > 0
        ? valueContext.find((v) => context.includes(v))
          ? true
          : false
        : true;

    return !value ? { display: "none" } : null;
  };

  const displayContext1 = displayCell([1], context);
  const displayContext2 = displayCell([1, 2], context);
  const displayContext3 = displayCell([1, 2, 3], context);
  const displayContext1Et3 = displayCell([1, 3], context);
  const displayContext1Et4 = displayCell([1, 4], context);

  return (
    <Paper sx={{ width: "100%", padding: 5, margin: 5 }}>
      <TableContainer sx={{ width: "100%" }}>
        <Table stickyHeader aria-label="sticky table">
          <TableRow>
            <ProductCell data={data.map((d) => d.produit)} />

            {getNberOfMagasin().map((index) => {
              return (
                <MagasinCell
                  context={{
                    context1: displayContext1,
                    context3: displayContext3,
                    context1Et2: displayContext2,
                  }}
                  index={index}
                  key={index}
                  magasins={data.map((item) => item.magasins[index])}
                />
              );
            })}

            {afficherTransfertEtRetour.transfert && (
              <TransfertCellEtRetour
                title="Transfert"
                data={data.map((d) => d.transfert)}
                context={{ context: displayContext1Et3 }}
              />
            )}
            {afficherTransfertEtRetour.retour && (
              <TransfertCellEtRetour
                title="Retour"
                data={data.map((d) => d.retour)}
                context={{ context: displayContext1Et4 }}
              />
            )}
          </TableRow>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={[].length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={(e) => {
          console.log(e);
        }}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Paper>
  );
}
