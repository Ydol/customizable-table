import { Table, TableBody, TableHead, TableRow } from "@mui/material";
import { CellHeader, styleSubHeader } from "./table.css";
import { Cell, CellHead, CellSubHead } from "./TableRedefineCell";
import { TransfertRetour } from "../data";

interface Props {
  title: string;
  data: TransfertRetour[];
  context: {
    context: { display: string } | null;
  };
}

const TransfertCellEtRetour = (props: Props) => {
  const { title, data, context } = props;
  return (
    <Cell sx={{ ...CellHeader }}>
      <Table>
        <TableHead>
          <TableRow>
            <CellHead sx={{ ...context.context }}></CellHead>
            <CellHead sx={{ ...context.context }}>{title}</CellHead>
            <CellHead sx={{ ...context.context }}></CellHead>
            <CellHead sx={{ ...context.context }}></CellHead>
          </TableRow>
        </TableHead>
        <TableBody sx={{ backgroundColor: "#fff1d0" }}>
          <TableRow sx={{ ...styleSubHeader }}>
            <CellSubHead sx={{ ...context.context }}>Depuis (Mag)</CellSubHead>
            <CellSubHead sx={{ ...context.context }}>Vers (Mag)</CellSubHead>
            <CellSubHead sx={{ ...context.context }}>
              Commentaire {title}
            </CellSubHead>
            <CellSubHead sx={{ ...context.context }}>Qte UA</CellSubHead>
          </TableRow>
          {data.map((item, index) => (
            <TableRow sx={{ height: "90px" }} key={index}>
              <Cell sx={{ ...context.context }}>{item.depuis}</Cell>
              <Cell sx={{ ...context.context }}>{item.vers}</Cell>
              <Cell sx={{ ...context.context }}>{item.commentaire}</Cell>
              <Cell sx={{ ...context.context }}>{item.qte}</Cell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Cell>
  );
};

export default TransfertCellEtRetour;
