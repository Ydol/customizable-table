import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from "@mui/material";
import { CellHeader, styleSubHeader } from "./table.css";
import { Cell, CellHead, CellSubHead } from "./TableRedefineCell";
import { Produit } from "../data";

interface Props {
  data: Produit[];
}

const ProductCell = (props: Props) => {
  const { data } = props;
  return (
    <TableCell width={"20px"} sx={{ ...CellHeader, border: "none" }}>
      <Table>
        <TableHead sx={{ fontWeight: "bold" }}>
          <TableRow>
            <CellHead></CellHead>
            <CellHead></CellHead>
            <CellHead></CellHead>
            <CellHead></CellHead>
            <CellHead></CellHead>
            <CellHead></CellHead>
            <CellHead></CellHead>
            <CellHead sx={{ fontWeight: "bold" }}>Bloc{">>>"}</CellHead>
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow sx={{ ...styleSubHeader }}>
            <CellSubHead>Code</CellSubHead>
            <CellSubHead>Libelle</CellSubHead>
            <CellSubHead>DPA Cab</CellSubHead>
            <CellSubHead>DPA Appli</CellSubHead>
            <CellSubHead>D vendeur Appli</CellSubHead>
            <CellSubHead>PV Cab</CellSubHead>
            <CellSubHead sx={{transform: 'rotate(-90deg)'}}>Statut</CellSubHead>
            <CellSubHead sx={{ backgroundColor: "#fff1d0" }}>
              Commentaire Projet
            </CellSubHead>
          </TableRow>
          {data.map((item) => {
            return (
              <TableRow sx={{ height: "90px" }} key={item.codeArticle}>
                <Cell>{item.codeArticle}</Cell>
                <Cell>{item.libelle}</Cell>
                <Cell>{item.dPACAB}</Cell>
                <Cell>{item.dPAAPPLI}</Cell>
                <Cell>{item.dVendAppli}</Cell>
                <Cell>{item.pvCAB}</Cell>
                <Cell>{item.statut}</Cell>
                <Cell sx={{ backgroundColor: "#fff1d0" }}>
                  {item.commentaire}
                </Cell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </TableCell>
  );
};

export default ProductCell;
