import { TableCell, TableCellProps } from "@mui/material";

export const Cell = (props: TableCellProps) => {
  return (
    <TableCell
      padding="checkbox"
      {...props}
      sx={{ ...props.sx, border: "1px solid black" }}
    />
  );
};

export const CellHead = (props: TableCellProps) => {
  return (
    <TableCell
      size="small"
      {...props}
      sx={{
        ...props.sx,
        fontWeight: "bold",
        borderBottom: "1px solid black",
      }}
    />
  );
};

export const CellSubHead = (props: TableCellProps) => {
  return (
    <TableCell
      padding="checkbox"
      {...props}
      sx={{
        ...props.sx,
        border: "1px solid black",
        fontWeight: "bold",
      }}
    />
  );
};
