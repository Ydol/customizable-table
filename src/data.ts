export interface Produit {
  codeArticle: string;
  libelle: string;
  detailPCB: string;
  ua: string;
  dPACAB: string;
  pvCAB: string;
  dPAAPPLI: string;
  dVendAppli: string;
  statut: string;
  commentaire: string;
}

export interface Magasin {
  id: string;
  name: string;
  invColis: string;
  invPiece: string;
  invConca: string;
}

export interface TransfertRetour {
  depuis: string;
  vers: string;
  commentaire: string;
  qte: string;
}
export interface Model {
  produit: Produit;
  magasins: Magasin[];
  transfert: TransfertRetour;
  retour: TransfertRetour;
}

export interface OptionFilter {
  title: string;
  value: string;
}

export const datas: Model[] = [
  {
    produit: {
      codeArticle: "0001",
      libelle: "SAC",
      detailPCB: "*12PC",
      ua: "c",
      dPACAB: "",
      pvCAB: "",
      dPAAPPLI: "",
      dVendAppli: "",
      statut: "",
      commentaire: "",
    },
    magasins: [
      {
        id: "magasin_1",
        name: "LAM",
        invColis: "7c",
        invPiece: "18",
        invConca: "M:18p, R:7c",
      },
      {
        id: "magasin_2",
        name: "VALI",
        invColis: "8c",
        invPiece: "17",
        invConca: "M:17p, R:8c",
      },
      // {
      //   name: "BLUE",
      //   invColis: "5c",
      //   invPiece: "10",
      //   invConca: "M:10p, R:5c",
      // },
      // {
      //   name: "RED",
      //   invColis: "4c",
      //   invPiece: "12",
      //   invConca: "M:12p, R:5c",
      // },
    ],
    transfert: {
      depuis: "LAM",
      vers: "VAL",
      commentaire: "string",
      qte: "5",
    },
    retour: {
      depuis: "VAL",
      vers: "LAM",
      commentaire: "string",
      qte: "4",
    },
  },
  {
    produit: {
      codeArticle: "0002",
      libelle: "FOULARD",
      detailPCB: "*11PC",
      ua: "c",
      dPACAB: "",
      pvCAB: "",
      dPAAPPLI: "",
      dVendAppli: "",
      statut: "",
      commentaire: "",
    },
    magasins: [
      {
        id: "magasin_1",
        name: "LAM",
        invColis: "6c",
        invPiece: "15",
        invConca: "M:15p, R:6c",
      },
      {
        id: "magasin_2",
        name: "VALI",
        invColis: "9c",
        invPiece: "19",
        invConca: "M:19p, R:9c",
      },
      // {
      //   name: "BLUE",
      //   invColis: "4c",
      //   invPiece: "9",
      //   invConca: "M:4p, R:9c",
      // },
      // {
      //   name: "RED",
      //   invColis: "6c",
      //   invPiece: "36",
      //   invConca: "M:36p, R:6c",
      // },
    ],
    transfert: {
      depuis: "",
      vers: "",
      commentaire: "",
      qte: "",
    },
    retour: {
      depuis: "",
      vers: "",
      commentaire: "",
      qte: "",
    },
  },
];
