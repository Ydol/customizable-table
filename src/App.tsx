import React, { useEffect } from "react";
import "./App.css";
import ColumnGroupingTable from "./Table";
import { Model, OptionFilter, datas } from "./data";
import {
  Autocomplete,
  AutocompleteChangeReason,
  Grid,
  TextField,
  Typography,
} from "@mui/material";

function App() {
  useEffect(() => {
    const magasins = datas[0].magasins.map((m) => ({
      title: m.name,
      value: m.id,
    }));
    setAffichageBloc([
      ...magasins,
      { title: "Transfert", value: "transfert" },
      { title: "Retour", value: "retour" },
    ]);
  }, []);

  const defaultAffichageContexte = [
    { title: "1", value: "1" },
    { title: "2", value: "2" },
    { title: "3", value: "3" },
    { title: "4", value: "4" },
  ];

  const [dataAfficher, setdataAfficher] = React.useState<Model[]>(datas);
  const [affichageBloc, setAffichageBloc] = React.useState<any[]>([]);
  const [afficherTransfertEtRetour, setAfficherTransfertEtRetour] =
    React.useState<{
      retour: boolean;
      transfert: boolean;
    }>({
      transfert: false,
      retour: false,
    });
  const [blocAfficher, setBlocAfficher] = React.useState<
    { title: string; value: "string" }[]
  >([]);
  const [affichageContexte, setAffichageContexte] = React.useState(
    defaultAffichageContexte
  );

  const [filtre, setFiltre] = React.useState({
    libelle: "",
    codeArticle: "",
    rayonProduit: "",
    familleProduit: "",
    sousFamilleProduit: "",
    calendrier: "",
    qteVide: 0,
    statutProduit: "",
    isExpanded: false,
    commandeClient: "",
    transfert: "",
    retour: "",
  });

  const filterOptions = (options: OptionFilter[]) => {
    const selectedValues = new Set(
      affichageContexte.map((option) => option.value)
    );
    return options.filter((option) => !selectedValues.has(option.value));
  };

  React.useEffect(() => {
    if (blocAfficher.length > 0) {
      let filter = [];
      const idmagasin: string[] = [];
      blocAfficher.forEach((element) => {
        if (element.value.includes("magasin")) {
          idmagasin.push(element.value);
        }
      });

      setAfficherTransfertEtRetour({
        transfert: blocAfficher.find((b) => b.value.includes("transfert"))
          ? true
          : false,
        retour: blocAfficher.find((b) => b.value.includes("retour"))
          ? true
          : false,
      });

      filter = datas.map((item) => {
        const magasins = item.magasins.filter((m) => idmagasin.includes(m.id));
        return {
          ...item,
          magasins: magasins,
        };
      });
      setdataAfficher(filter);
    } else {
      setAfficherTransfertEtRetour({
        transfert: true,
        retour: true,
      });
      setdataAfficher(datas);
    }
  }, [blocAfficher]);

  // Effectuer tous les filtres
  React.useEffect(() => {
    const filtered = datas.filter((item) => {
      const conditionCodeArticle = item.produit.codeArticle
        .toLowerCase()
        .includes(filtre.codeArticle.toLowerCase());

      const conditionLibelle = item.produit.libelle
        .toLowerCase()
        .includes(filtre.libelle.toLowerCase());

      const conditionStatut = item.produit.statut
        .toLowerCase()
        .includes(filtre.statutProduit.toLowerCase());

      return conditionCodeArticle && conditionLibelle && conditionStatut;
    });

    setdataAfficher(filtered);
  }, [filtre]);

  const handlerFilterChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setFiltre((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };

  return (
    <>
      <Grid container spacing={2} mb={10}>
        <Grid item md={5} xs={12}>
          <Grid container spacing={2}>
            <Grid item md={5} xs={12}>
              <Typography>No Liste</Typography>
            </Grid>
            <Grid item md={7} xs={12}>
              <TextField fullWidth size="small" placeholder="No liste" />
            </Grid>

            <Grid item md={5} xs={12}>
              <Typography>Tri</Typography>
            </Grid>
            <Grid item md={7} xs={12}>
              <TextField fullWidth size="small" placeholder="Tri" />
            </Grid>

            <Grid item md={5} xs={12}>
              <Typography>Affichage bloc</Typography>
            </Grid>
            <Grid item md={7} xs={12}>
              <Autocomplete
                multiple
                size="small"
                options={affichageBloc}
                getOptionLabel={(option) => option.title}
                filterSelectedOptions
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Affichage bloc"
                    placeholder="Affichage bloc"
                  />
                )}
                onChange={(
                  _event: React.SyntheticEvent<Element, Event>,
                  value: any[],
                  _reason: AutocompleteChangeReason
                ) => {
                  setBlocAfficher(value);
                }}
              />
            </Grid>

            <Grid item md={5} xs={12}>
              <Typography>Contexte d'affichage</Typography>
            </Grid>
            <Grid item md={7} xs={12}>
              <Autocomplete
                multiple
                size="small"
                options={defaultAffichageContexte}
                value={affichageContexte}
                getOptionLabel={(option) => option.title}
                filterSelectedOptions
                filterOptions={filterOptions}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Contexte d'affichage"
                    placeholder="Contexte d'affichage"
                  />
                )}
                onChange={(
                  _event: React.SyntheticEvent<Element, Event>,
                  value: OptionFilter[],
                  _reason: AutocompleteChangeReason
                ) => {
                  setAffichageContexte(value);
                }}
              />
            </Grid>
          </Grid>
        </Grid>

        <Grid item md={1} xs={12}/>
        <Grid
          item
          md={6}
          xs={12}
          // sx={{ border: "solid", borderRadius: "7px", borderWidth: "1px" }}
        >
          <Grid container spacing={1}>
            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                label="Code Article"
                size="small"
                value={filtre.codeArticle}
                name="codeArticle"
                onChange={(
                  e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
                ) => handlerFilterChange(e)}
              />
            </Grid>

            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                label="Libelle"
                size="small"
                value={filtre.libelle}
                name="libelle"
                onChange={(
                  e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
                ) => handlerFilterChange(e)}
              />
            </Grid>

            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                label="Famille produit"
                size="small"
                name="familleProduit"
                value={filtre.familleProduit}
                onChange={(
                  e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
                ) => handlerFilterChange(e)}
              />
            </Grid>

            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                label="Sous famille produit"
                size="small"
                name="sousFamilleProduit"
                value={filtre.sousFamilleProduit}
                onChange={(
                  e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
                ) => handlerFilterChange(e)}
              />
            </Grid>

            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                label="Statut produit"
                size="small"
                name="statutProduit"
                value={filtre.statutProduit}
                onChange={(
                  e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
                ) => handlerFilterChange(e)}
              />
            </Grid>

            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                type="number"
                label="Quantite vide"
                size="small"
                name="qteVide"
                value={filtre.qteVide}
                onChange={(
                  e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
                ) => handlerFilterChange(e)}
              />
            </Grid>

            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                label="Commande client"
                size="small"
                name="commandeClient"
                value={filtre.commandeClient}
                onChange={(
                  e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
                ) => handlerFilterChange(e)}
              />
            </Grid>

            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                label="Tansfert"
                size="small"
                value={filtre.transfert}
                name="transfert"
                onChange={(
                  e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
                ) => handlerFilterChange(e)}
              />
            </Grid>

            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                label="Retour"
                size="small"
                value={filtre.retour}
                name="retour"
                onChange={(
                  e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
                ) => handlerFilterChange(e)}
              />
            </Grid>
          </Grid>
        </Grid>
      </Grid>

      <ColumnGroupingTable
        data={dataAfficher}
        context={affichageContexte.map((v) => Number(v.value))}
        afficherTransfertEtRetour={afficherTransfertEtRetour}
      />
    </>
  );
}

export default App;
